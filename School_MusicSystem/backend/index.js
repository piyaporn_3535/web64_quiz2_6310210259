const bcrypt = require('bcrypt')
const SALT_ROUNDS = 10

const jwt = require('jsonwebtoken')
const dotenv = require('dotenv')
dotenv.config()
const TOKEN_SECRET = process.env.TOKEN_SECRET

const mysql = require('mysql');

const connection = mysql.createConnection({
    host : 'localhost',
    user : 'root',
    password : 'root',
    database : 'School_MusicSystem'
});

connection.connect();

const express = require('express');
const app = express()
const port = 4000

/*Middleware for Authenticating User Token */

function authenticateToken(req ,res, next) {
    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(' ')[1]
    if (token == null) return res.sendStatus(401)
    jwt.verify(token, TOKEN_SECRET, (err, user) =>{
        if (err) { return res.sendStatus(403) }
        else {
            req.user = user
            next()
        }
    })
}

/* API for Processing Music Authorization */
app.post("/login", (req, res) =>{
    let username = req.query.username
    let user_password = req.query.password
    let query = `SELECT * FROM Music WHERE Username = '${username}'`
    connection.query( query, (err,rows) =>{
                if (err) {
                    console.log(err)
                    res.json({
                        "status" : "400",
                        "message" : "Error querying from running db"
                        })
                }else {
                    let db_password = rows[0].Password
                    bcrypt.compare(user_password, db_password, (err, result) =>{
                        if (result){
                           let payload = {
                               "username" : rows[0].Username,
                               "user_id" : rows[0].MusicID,
                           }

                           let token = jwt.sign(payload, TOKEN_SECRET, { expiresIn : '1d' } )
                           res.send(token)
                        }else { res.send ("Invalid username / password")}
                    })
                

                } 
    })
})

/*API for Registering a new Runner */
app.post("/register_music", (req,res) =>{
    let music_name  = req.query.music_name
    let music_surname = req.query.music_surname
    let music_username = req.query.music_username
    let music_password = req.query.music_password
    
    bcrypt.hash(music_password, SALT_ROUNDS, (err, hash) => {
        let query = `INSERT INTO  Music 
                 (MusicName, MusicSurname, Username, Password)
                 VALUES ('${music_name}','${music_surname}',
                         '${music_username}', '${hash}')`
        
        console.log(query)

        connection.query( query,(err, rows) =>{
                if (err) {
                    res.json({
                        "status" : "400",
                        "message" : "Error inserting data into db"
                        })
                }else {
                    res.json ({
                            "status" : "200",
                            "message" : "Adding new user succesful"
                    })
            }
        });

    })
        

    
 })

/* CRUD  Operation for SchoolMusic Table */
app.get("/list_school",authenticateToken , (req, res) => {
    query = "SELECT * from SchoolMusic";
connection.query( query, (err, rows) => {
        if (err) {
                res.json({
                            "status" : "400",
                            "message" : "Error querying from running db"
                        })
        }else {
            res.json(rows)
        }
    });
})

app.post("/update_music",authenticateToken,(req,res) =>{
    let music_id = req.query.music_id
    let music_name  = req.query.music_name
    let music_surname = req.query.music_surname

    let query = `UPDATE Music SET 
                 MusicName = '${music_name}',
                 MusicSurname = '${music_surname}'
                 WHERE MusicID  = ${music_id}`

    console.log(query)

    connection.query( query,(err, rows) =>{
        if (err) {
            console.log(err)
            res.json({
                   "status" : "400",
                   "message" : "Error updating record"
            })
        }else {
            res.json ({
                    "status" : "200",
                    "message" : "Updating music succesful"
            })
        }
    });
})

app.post("/add_school", (req, res)  =>  {

    let school_name = req.query.school_name
    let school_location = req.query.school_location
    let school_type = req.query.school_type
    let query = `INSERT INTO SchoolMusic
                (SchoolName, SchoolLocation,SchoolType)
                VALUES ('${school_name}','${school_location}','${school_type}')`
    console.log(query)

    connection.query( query, (err, rows) => {
        if (err) {
            res.json({
                "status" : "400",
                "message" : "Error inserting data into db"
                    })
        }else {
            res.json({
                "status" : "200",
                "message" : "Adding event succesful"
            })
        }
    });
    
})

app.post("/delete_music", (req, res)  =>  {

    let music_id = req.query.music_id
   

    let query = `DELETE FROM Music WHERE MusicID=${music_id}`

    console.log(query)

    connection.query( query, (err, rows) => {
        if (err) {
            console.log(err)
            res.json({
                "status" : "400",
                "message" : "Error deleting record"
                    })
        }else {
            res.json({
                "status" : "200",
                "message" : "Deleting record succesful"
            })
        }
    });

})

app.listen(port, () => {
    console.log(`Now starting Running System Backend ${port} `)
})
